import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,Button,
  View
} from 'react-native';

import {createStackNavigator} from 'react-navigation'



export default class HomeScren extends Component {
    static navigationOptions = {
        title: 'QR Code Scanner app',
      };

  render() {
    return (
      <View style={styles.container}>
        
        <Button
          title="Go to QR Code "
          onPress={() => this.props.navigation.navigate('CodeScreen')}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }
});



import React, { Component } from 'react';
import { StyleSheet, Text, Linking, View, TouchableOpacity, Button } from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import UrlValidator from '../utils/url-validator';



export default class CodeScannerScreen extends Component {
    static navigationOptions = {
        title: 'go back',
    };

    scanner = null;
    onSuccess(e) {
        if (UrlValidator.validate(e.data)) {
            Linking
                .openURL(e.data)
                .catch(err => alert('An error occured' + err))
        } else {
            alert('please scan a valide qr code that has a valide url');
        }

    }

    render() {
        return (
            <View style={styles.container}>
                <QRCodeScanner
                    ref={(node) => this.scanner = node}
                    onRead={this.onSuccess.bind(this)}
                    bottomContent={
                        <Button onPress={()=> this.scanner.reactivate()}
                            title='scan again'/>
                    }
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    }
});



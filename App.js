/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,TouchableOpacity,
  View
} from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner'
import HomeScreen from './src/screens/homeScreen'
import CodeScannerScreen from './src/screens/codeScannerScreen'
import {createStackNavigator} from 'react-navigation'


export default class App extends Component {
  render() {
    return (
      <RootStackNavigator />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }
});


const RootStackNavigator = createStackNavigator(
  {
    Home: HomeScreen,
    CodeScreen: CodeScannerScreen
  },
  {
    initialRouteName: 'Home',
  }
)
